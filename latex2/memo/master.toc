\babel@toc {spanish}{}\relax 
\contentsline {section}{\numberline {1}Introducción}{3}{section.1}%
\contentsline {section}{\numberline {2}Análisis Exploratorio y Tratamiento de Datos}{3}{section.2}%
\contentsline {subsection}{\numberline {2.1}Descripción de la estructura de los datos}{3}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}Identificación de datos faltantes}{5}{subsection.2.2}%
\contentsline {subsection}{\numberline {2.3}Identificación de relaciones entre variables}{5}{subsection.2.3}%
\contentsline {subsubsection}{\numberline {2.3.1}Histograma fabricante}{5}{subsubsection.2.3.1}%
\contentsline {subsubsection}{\numberline {2.3.2}Distribución del precio}{6}{subsubsection.2.3.2}%
\contentsline {subsubsection}{\numberline {2.3.3}Variables numéricas}{7}{subsubsection.2.3.3}%
\contentsline {subsubsection}{\numberline {2.3.4}Variables categóricas}{7}{subsubsection.2.3.4}%
\contentsline {subsection}{\numberline {2.4}Predictores Cualitativos}{8}{subsection.2.4}%
\contentsline {section}{\numberline {3}Selección del Modelo}{8}{section.3}%
\contentsline {subsection}{\numberline {3.1}Baseline}{9}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Backward y Forward}{9}{subsection.3.2}%
\contentsline {subsubsection}{\numberline {3.2.1}Backward}{9}{subsubsection.3.2.1}%
\contentsline {subsubsection}{\numberline {3.2.2}Forward}{9}{subsubsection.3.2.2}%
\contentsline {subsection}{\numberline {3.3}Best Subset}{9}{subsection.3.3}%
\contentsline {subsection}{\numberline {3.4}Resultado}{9}{subsection.3.4}%
\contentsline {section}{\numberline {4}Diagnóstico del Modelo}{9}{section.4}%
\contentsline {section}{\numberline {5}Evaluación del Modelo}{10}{section.5}%
\contentsline {section}{\numberline {6}Conclusión}{10}{section.6}%
