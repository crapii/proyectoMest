library(stringr)
library(GGally)

data <- read.csv("./CarPrice_Assignment.csv", header = TRUE)
head(data)
summary(data)

### Análisis Exploratorio/Descriptivo de los datos
## Limpieza
# Eliminamos car_ID (no sirve)

data <- data[-1]

# Calculamos los valores que faltan

which(is.na(data))

# Corregir nombres de marcas

data$CarName
CarNameNew <- sapply(strsplit(data$CarName," +"), `[`, 1)
CarNameNew
unique(CarNameNew)
data$CarName <- CarNameNew
data$CarName

rep_str = c('maxda'='mazda', 'nissan'='Nissan', 'porcshce'='porsche', 'toyouta'='toyota', 'vokswagen'='volkswagen', 'vw'='volkswagen')

data$CarName <- str_replace_all(data$CarName, rep_str)
data$CarName

# Cambiar tipo para symboling (es variable categorica)
# symboling denota el riesgo asociado al precio del coche con valores -3, -2, -1, 0, 1, 2, 3

typeof(data$symboling)
data$symboling
data$symboling <- toString(data$symboling)
data$symboling
typeof(data$symboling)

# Datos duplicados (no hay)

data[duplicated(data) | duplicated(data, fromLast=TRUE), ]


## Visualización datos

# Diferentes marcas de coches (toyota max y mercuri min)

carName <- table(data$CarName)
barplot(carName,
   xlab="Marcas de Coches", ylab="Número de Coches") 

ggplot(data, aes(x=reorder(CarName, CarName, function(x)-length(x)))) +
  geom_bar(fill='steelblue') +
  labs(x='Fabricante', y='Frecuencia')

# Distribución del precio (sesgada hacia la izquierda)

x <- data$price
hist(x, breaks=20, main="Histograma Precio",xlab="Precio ($)", ylab="Frecuencia")
plot(density(x), main="Histograma Precio",xlab="Precio ($)", ylab="Frecuencia")

hist(data$price)
lines(density(data$price))

hist(data$price, breaks = "Scott")

lines(density(data$price),
      lwd = 2,
      col = "chocolate3")

ggplot(data, aes(x=price)) + 
  geom_histogram(aes(y=after_stat(density)),      # Histogram with density instead of count on y-axis
                 binwidth=1000,
                 colour="black", fill="white") +
  geom_density(alpha=.5, fill="#FF6666")  # Overlay with transparent density plot


# Visualización variables numéricas 
# ( carwidth carlength curbweight enginesize horsepower : correlación positiva) 
# ( carheigth : no hay relación)
# ( citympg highwaympg : correlación negaitva)

ggpairs(Filter(is.numeric, data))
pairs(Filter(is.numeric, data))

# Visualización del resto de variables categoricas

which(sapply(data, is.character) == TRUE)

# Observaciones: 
# doornumber, no afecta al precio en gran medida
# fueltype, diesel es más caro que gasolina
# aspiration, turbo es más caro que std
# (Nota: coches con turbo tiene más caballos)
# carbody, convertible y hardtop son los más caros
# enginelocation, rear engine más caro que front engine
# drivewheel, tracción trasera más cara que delantera
# enginetype, ohcv más caro
# cylindernumber, más cilindros más caro
# fuelsystem, ni idea

g1 <- ggplot(data, aes(doornumber, y=price)) + geom_boxplot()
g2 <- ggplot(data, aes(fueltype, y=price)) + geom_boxplot()
g3 <- ggplot(data, aes(aspiration, y=price)) + geom_boxplot()
g4 <- ggplot(data, aes(carbody, y=price)) + geom_boxplot()
g5 <- ggplot(data, aes(enginelocation, y=price)) + geom_boxplot()
g6 <- ggplot(data, aes(drivewheel, y=price)) + geom_boxplot()
g7 <- ggplot(data, aes(enginetype, y=price)) + geom_boxplot()
g8 <- ggplot(data, aes(cylindernumber, y=price)) + geom_boxplot()
g9 <- ggplot(data, aes(fuelsystem, y=price)) + geom_boxplot()

library(gtable)
install.packages('gridExtra')
library(gridExtra)

grid.arrange(g1, g2, g3, g4, g5, g6, g7, g8, g9, nrow = 3)

# El tipo de coche que aparace con mayor frecuencia es sedan
# La mayor parte de los coches tienen cuatro cilindros
# La mayor parte de los coches son de gasolina

f1 <- ggplot(data, aes(x=reorder(carbody, carbody, function(x)-length(x)))) +
  geom_bar(fill='steelblue') +
  labs(x='Team')
f2 <- ggplot(data, aes(x=reorder(cylindernumber, cylindernumber, function(x)-length(x)))) +
  geom_bar(fill='steelblue') +
  labs(x='Team')
f3 <- ggplot(data, aes(x=reorder(fueltype, fueltype, function(x)-length(x)))) +
  geom_bar(fill='steelblue') +
  labs(x='Team')

grid.arrange(f1, f2, f3, nrow=1)

h1 <- ggplot(data, aes(fuelsystem, y=price, fill=fueltype)) + 
  geom_boxplot()
h2 <- ggplot(data, aes(carbody, y=price, fill=enginelocation)) + 
  geom_boxplot()
h3 <- ggplot(data, aes(cylindernumber, y=price, fill=fueltype)) + 
  geom_boxplot()

grid.arrange(h1, h2, h3, nrow=1)

# Average price vs Carname (jaguar, buick y porsche tienen mayor precio medio)

df1 <- data %>% group_by(CarName) %>% summarise(me = mean(price))
ggplot(aes(x = reorder(CarName, -me), y = me), data = df1) +
  geom_bar(stat = "identity")

# Average price vs carbody

df2 <- data %>% group_by(carbody) %>% summarise(me = mean(price))
ggplot(aes(x = reorder(carbody, -me), y = me), data = df2) +
  geom_bar(stat = "identity")

## Variables significativas después de Visualización
# ¿Agrupar datos por categorías de precio económico, medio, lujo?

library(dplyr)
df3 <- data %>% 
    group_by(gr=cut(price, breaks= seq(0, max(price), by = 10000)) ) %>% 
    summarise(n= n()) %>%
    arrange(as.numeric(gr))

df4 <- data %>%
  mutate(pricerange = ifelse(price > 40000 , "very high", 
                             ifelse(price > 20000 & price < 30000, "medium", 
                             ifelse(price < 20000, "low", "high"))))

ggplot(df4, aes(pricerange, y=price)) + 
  geom_boxplot()

# pricerange, enginetype, fueltype, carbody, aspiration, cylindernumber
# drivewheel, curbweight, carlength, carwidth, enginesize
# boreratio, horsepower, wheelbase, citympg, highwaympg, symboling

var <- c("pricerange", "enginetype", "fueltype", "carbody", "aspiration", "cylindernumber", "drivewheel", "curbweight", "carlength", "carwidth", "enginesize", "boreratio", "horsepower", "wheelbase", "citympg", "highwaympg", "symboling")

### Preparación de los datos

library('dplyr')
df5 <- df4 %>% select(all_of(var))
head(df5)

## Dummy Variable

df6 <- df5
head(df6)

install.packages("fastDummies")
library(fastDummies)

dummies <- c('enginetype', 'carbody', 'fueltype', 'aspiration', 'carbody', 'drivewheel', 'cylindernumber')
df6 <- dummy_cols(df6, select_columns = dummies)
df6 <- df6 %>% select(-dummies)
head(df6)
length(df6)
